use std::net::{TcpListener, TcpStream};

extern crate serde;
extern crate serde_json;
use serde::{Serialize,Deserialize};
use std::io::Write;

#[derive(Serialize, Deserialize, Debug)]
struct TextMessage {
    text: String
}

impl TextMessage {
    pub fn new(text: String) -> TextMessage {
        TextMessage {
            text
        }
    }
}

fn main() {
    let message = TextMessage::new("test!".to_owned());

    let serialized = serde_json::to_string(&message).unwrap();
    println!("serialized = {}", serialized);

    let listener = TcpListener::bind("127.0.0.1:7171").unwrap();
    for stream in listener.incoming() {
        let mut conn = stream.unwrap();
        conn.write_all(serialized.as_bytes()).unwrap();
        conn.flush().unwrap();
    }

}
